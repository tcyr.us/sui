const data_src = "https://postgrest.tcyr.us/";
const data_links = data_src + "sui_category_links",
      data_apps = data_src + "sui_apps",
      data_providers = data_src + "sui_providers";

$(function() {
  $.getJSON(data_links, function(data) {
    const source = document.getElementById('links-template').innerHTML;
    const template = Handlebars.compile(source);
    const result = template({bookmarks: data});
    document.getElementById('links').innerHTML = result;
  });

  $.getJSON(data_apps, function(data) {
    const source = document.getElementById('apps-template').innerHTML;
    const template = Handlebars.compile(source);
    const result = template({apps: data});
    document.getElementById('apps').innerHTML = result;
  });

  $.getJSON(data_providers, function(data) {
    const source = document.getElementById('providers-template').innerHTML;
    const template = Handlebars.compile(source);
    const result = template({providers: data});
    document.getElementById('providers').innerHTML = result;
  });
});
