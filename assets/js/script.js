function date() {
  const currentDate = new Date();
  const dateOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric"
  };
  const date = currentDate.toLocaleDateString("en-GB", dateOptions);
  document.getElementById("header_date").innerHTML = date;
}

function greet() {
  const currentTime = new Date();
  const greet = Math.floor(currentTime.getHours() / 6);

  let greeting = '';
  switch (greet) {
    case 0:
      greeting = "Good night!";
      break;
    case 1:
      greeting = "Good morning!";
      break;
    case 2:
      greeting = "Good afternoon!";
      break;
    case 3:
      greeting = "Good evening!";
      break;
  }

  document.getElementById("header_greet").innerHTML = greeting;
}

$(function() {
  date();
  greet();
});
