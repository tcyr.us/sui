const setValue = function(property, value) {
    if (value) {
        document.documentElement.style.setProperty(`--${property}`, value);

        const input = document.getElementById(property);
        if (input) {
            input.value = value.replace('px', '');
        }
    }
};

const setValueFromLocalStorage = function(property) {
    const value = localStorage.getItem(property);
    setValue(property, value);
};

const setTheme = function(options) {
    for (const [property, value] of Object.entries(options)) {
        setValue(property, value);
        localStorage.setItem(property, value);
    }
}

$(function() {
    setValueFromLocalStorage('color-background');
    setValueFromLocalStorage('color-text-pri');
    setValueFromLocalStorage('color-text-acc');
});

const dataThemeButtons = document.querySelectorAll('[data-theme]');

for (const dataThemeButton of dataThemeButtons) {
    dataThemeButton.addEventListener('click', function() {
        const theme = dataThemeButton.dataset.theme;

        switch (theme) {
            case 'blackboard':
                setTheme({
                    'color-background': '#1a1a1a',
                    'color-text-pri': '#FFFDEA',
                    'color-text-acc': '#5c5c5c'
                });
                break;

            case 'gazette':
                setTheme({
                    'color-background': '#F2F7FF',
                    'color-text-pri': '#000',
                    'color-text-acc': '#5c5c5c'
                });
                break;

            case 'espresso':
                setTheme({
                    'color-background': '#21211F',
                    'color-text-pri': '#D1B59A',
                    'color-text-acc': '#4E4E4E'
                });
                break;

            case 'cab':
                setTheme({
                    'color-background': '#F6D305',
                    'color-text-pri': '#1F1F1F',
                    'color-text-acc': '#424242'
                });
                break;

            case 'cloud':
                setTheme({
                    'color-background': '#f1f2f0',
                    'color-text-pri': '#35342f',
                    'color-text-acc': '#37bbe4'
                });
                break;

            case 'lime':
                setTheme({
                    'color-background': '#263238',
                    'color-text-pri': '#AABBC3',
                    'color-text-acc': '#aeea00'
                });
                break;

            case 'white':
                setTheme({
                    'color-background': '#FFF',
                    'color-text-pri': '#222',
                    'color-text-acc': '#DDD'
                });
                break;

            case 'tron':
                setTheme({
                    'color-background': '#242B33',
                    'color-text-pri': '#EFFBFF',
                    'color-text-acc': '#6EE2FF'
                });
                break;
            
            case 'blues':
                setTheme({
                    'color-background': '#2B2C56',
                    'color-text-pri': '#EFF1FC',
                    'color-text-acc': '#6677EB'
                });
                break;
            
            case 'passion':
                setTheme({
                    'color-background': '#f5f5f5',
                    'color-text-pri': '#12005e',
                    'color-text-acc': '#8e24aa'
                });
                break;
            
            case 'chalk':
                setTheme({
                    'color-background': '#263238',
                    'color-text-pri': '#AABBC3',
                    'color-text-acc': '#FF869A'
                });
                break;
            
            case 'paper':
                setTheme({
                    'color-background': '#F8F6F1',
                    'color-text-pri': '#4C432E',
                    'color-text-acc': '#AA9A73'
                });
                break;
        }
    })
}